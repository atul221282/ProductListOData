/* global module */
module.exports = function (grunt) {
    grunt.initConfig({
        connect: {
            first: {
                options: {
                    port: 9000,
                    base: "./"
                }
            },
            second: {
                options: {
                    open: {
                        target: "http://localhost:8001"
                    },
                    keepalive: true,
                    port: 8001,
                    livereload: 3501,
                    base: "./",
                }
            }
        },
    });

    grunt.loadNpmTasks("grunt-contrib-connect");
    grunt.registerTask("default", [
        "connect:second"
    ]);
};